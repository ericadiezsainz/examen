<?php
namespace clases;
class Imagen
{
    public string $src;
    public bool $border;
    public ?int $ancho;
    public ?int $alto;

    const RUTA = "./imagenes/";

    public function __construct(string $src="", bool $border=false, int $ancho=null, int $alto=null)

    {

        $this->src = $src;
        $this->border = $border;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }

    public function __toString()
    {
        return ("<img src='" . self::RUTA . $this->src . "' border='" . $this->border . "' width='" . $this->ancho . "' height='" . $this->alto . "'/>");
    }
   
          
        
    }

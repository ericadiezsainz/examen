<?php
namespace clases;
class Imagen
{
   private string $src;
   private bool $border;
   private ?int $ancho;
   private ?int $alto;

    const RUTA = "./imagenes/";

    public function __construct(string $src="", bool $border=false, int $ancho=null, int $alto=null)

    {

        $this->src = $src;
        $this->border = $border;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }

    public function __toString()
    {
        return ("<img src='" . self::RUTA . $this->src . "' border='" . $this->border . "' width='" . $this->ancho . "' height='" . $this->alto . "'/>");
    }
  
     

   /**
    * Get the value of src
    */ 
   public function getSrc()
   {
      return $this->src;
   }

   /**
    * Set the value of src
    *
    * @return  self
    */ 
   public function setSrc($src)
   {
      $this->src = $src;

      return $this;
   }

        /**
         * Get the value of border
         */ 
        public function getBorder()
        {
                return $this->border;
        }

        /**
         * Set the value of border
         *
         * @return  self
         */ 
        public function setBorder($border)
        {
                $this->border = $border;

                return $this;
        }

   /**
    * Get the value of ancho
    */ 
   public function getAncho()
   {
      return $this->ancho;
   }

   /**
    * Set the value of ancho
    *
    * @return  self
    */ 
   public function setAncho($ancho)
   {
      $this->ancho = $ancho;

      return $this;
   }

   /**
    * Get the value of alto
    */ 
   public function getAlto()
   {
      return $this->alto;
   }

   /**
    * Set the value of alto
    *
    * @return  self
    */ 
   public function setAlto($alto)
   {
      $this->alto = $alto;

      return $this;
   }
    }

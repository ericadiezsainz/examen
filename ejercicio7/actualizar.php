<?php
session_start();

require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");
$tabla = "libros";

controlErrores();


$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];
$menu = menu($elementosMenu);
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);


if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

$salida = "";


if (isset($_GET["id"])) {

    $_SESSION["id"] = $_GET["id"];
 
    $sql = "select * from {$tabla} where id={$_GET["id"]}";
    $resultado = $conexion->query($sql);
    $datos = $resultado->fetch_assoc();
}

if ($_POST) {
    foreach ($_POST as $campo => $valor) {
        $datos[$campo] = $valor;
    }
     $sql = "UPDATE {$tabla} SET titulo='{$datos['titulo']}', paginas={$datos['paginas']}, fechaPublicacion='{$datos['fechaPublicacion']}' WHERE id={$_SESSION['id']}";

    if ($resultado = $conexion->query($sql)) {

        $sql = "select * from {$tabla} where id={$_SESSION['id']}";

        $resultado = $conexion->query($sql);
        $salida = gridView($resultado);
        $salida .= "<br><br>Registro actualizado correctamente";
    } else {
        $salida = "Error al actualizar el registro" . $conexion->error;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1> <?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Actualizar </h1>

    <?= $menu ?>

    <?php
    if (!$_POST) {
        require "_form.php";
    }
    ?>


    <?= $salida ?>

</body>

</html>
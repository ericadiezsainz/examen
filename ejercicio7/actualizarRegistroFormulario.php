<?php

$parametros = require_once "parametros.php";

require_once "funciones.php";

// desactivar errores
controlErrores();

$salida = "";
$accion = "Actualizar";

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["servidor"],
    $parametros["usuario"],
    $parametros["password"],
    $parametros["nombreBd"]
);

if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}
if ($_POST) {
    // leer todos los datos del formulario
    $datos["id"] = $_POST["id"];
    $datos["titulo"] = $_POST["titulo"];
    $datos["paginas"] = $_POST["paginas"];
    $datos["fechaPublicacion"] = $_POST["fechaNacimiento"];

    $sql = "UPDATE empleados e 
        SET 
            nombre = '{$datos["nombre"]}', 
            apellidos = '" . $datos["apellidos"] . "', 
            edad = " . $datos["edad"] . ", 
            poblacion = '" . $datos["poblacion"] . "', 
            codigoPostal = '" . $datos["codigoPostal"] . "', 
            fechaNacimiento = '" . $datos["fechaNacimiento"] . "'
            WHERE id = {$datos["id"]}";

    if ($conexion->query($sql)) {
        $salida = "Registro actualizado correctamente";
        $salida .= "<br><a href='008-listarRegistrosUpdateDelete.php'>Volver a mostrar todos los registros</a>";
    } else {
        $salida = "Error al actualizar el registro: " . $conexion->error;
    }
}
if (isset($_GET["id"])) 
{
 
    $sql = "select * from empleados where id = " . $_GET["id"];

  
    $resultados = $conexion->query($sql);

 
    $datos = $resultados->fetch_assoc();
}

$conexion->close();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $parametros["nombreAplicacion"] ?></title>
</head>

<body>
    <h1><?= $parametros["nombreAplicacion"] ?></h1>
    <div>
        <?= $salida ?>
    </div>
    <br>
    <div>
        <?php require "_form.php" ?>
    </div>

</body>

</html>
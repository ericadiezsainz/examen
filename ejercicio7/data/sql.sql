DROP DATABASE IF EXISTS biblioteca;
CREATE DATABASE biblioteca;
USE biblioteca;

CREATE TABLE libros(
id int AUTO_INCREMENT,
titulo varchar(200),
paginas int,
fechaPublicacion date,
PRIMARY KEY(id)
);

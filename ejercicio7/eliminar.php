<?php
session_start();

require_once "funciones.php";

$parametros = require_once("parametros.php");
$tabla = "libros";

controlErrores();

$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];


$menu = menu($elementosMenu);

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}
$salida = "";

if (isset($_GET["id"])) {

    $sql = "delete from {$tabla} where id={$_GET["id"]}";
    if ($resultado = $conexion->query($sql)) {
        $salida = "Registro eliminado correctamente";
    } else {
        $salida = "Error al eliminar el registro" . $conexion->error;
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1> <?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Eliminar </h1>

    <?= $menu ?>

    <?= $salida ?>

</body>

</html>
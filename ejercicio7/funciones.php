<?php

function controlErrores()
{
    
    mysqli_report(MYSQLI_REPORT_OFF);
}

function gridView(mysqli_result $resultados)
{
    if ($resultados->num_rows > 0) {
        $registros = $resultados->fetch_all(MYSQLI_ASSOC);
        $salida = "<table border='1'>";
        $salida .= "<thead><tr>";
        $campos = array_keys($registros[0]);
        foreach ($campos as $campo) {
            $salida .= "<td>$campo</td>";
        }
        $salida .= "</tr></thead>";
        foreach ($registros as $registro) {
            $salida .= "<tr>";
            foreach ($registro as $campo => $valor) {
                $salida .= "<td>" . $valor . "</td>";
            }
            $salida .= "</tr>";
        }
        $salida .= "</table>";
    } else {
        $salida = "No hay registros";
    }
    return $salida;
}

function gridViewBotones(mysqli_result $resultados, array $botones): string
{
    if ($resultados->num_rows > 0) {
        $registros = $resultados->fetch_all(MYSQLI_ASSOC);
        // se muestran los registros
        $salida = "<table border='1'>";
        $salida .= "<thead><tr>";
        $campos = array_keys($registros[0]);
        foreach ($campos as $campo) {
            $salida .= "<td>$campo</td>";
        }
        
        $salida .= "<td>Acciones</td>";
        $salida .= "</tr></thead>";
        // muestro todos los registros
        foreach ($registros as $registro) {
            $salida .= "<tr>";
        foreach ($registro as $campo => $valor) {
                $salida .= "<td>" . $valor . "</td>";
            }
            $salida .= "<td>";

            foreach ($botones as $label => $enlace) {
                $salida .= "<a href='{$enlace}?id={$registro['id']}'>{$label}</a> | ";
            }

            $salida .= "</td>";

            $salida .= "</tr>";
        }
        $salida .= "</table>";
    } else {
        $salida = "No hay registros";
    }
    return $salida;
}

function menu(array $elementos): string
{
    $salida = "<ul>";

    foreach ($elementos as $label => $enlace) {
        $salida .= "<li><a href='{$enlace}'>{$label}</a></li>";
    }

    $salida .= "</ul>";
    return $salida;
}

<?php

require_once "funciones.php";

$tabla = "libros";

$parametros = require_once("parametros.php");

controlErrores();

$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];


$menu = menu($elementosMenu);


$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}


$sql = "select * from {$tabla}";


if ($resultado = $conexion->query($sql)) {
   
    $salida = gridViewBotones($resultado, [
        "Editar" => "actualizar.php",
        "Eliminar" => "eliminar.php"
    ]);
} else {
    $salida = "Error al ejecutar la consulta: " . $conexion->error;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1> <?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Inicio</h1>
    <?= $menu ?>
    <?= $salida ?>
</body>

</html>
<?php
require_once "funciones.php";

$parametros = require_once("parametros.php");

controlErrores();

$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

$menu = menu($elementosMenu);

$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

$salida = "";

$datos = [
    "titulo" => "",
    "paginas" => 0,
    "fechaPublicacion" => ""
];


if ($_POST) {
 
    foreach ($datos as $clave => $valor) {
        $datos[$clave] = $_POST[$clave];
    }

    $query = "INSERT INTO libros(titulo,paginas,fechaPublicacion) 
    VALUES ('
    {$datos["titulo"]}',
    {$datos["paginas"]},
    '{$datos["fechaPublicacion"]}')";

   
    if ($conexion->query($query)) {
        $salida = "Registro insertado";
       
        $sql = "select * from libros where id={$conexion->insert_id}";

        $resultado = $conexion->query($sql);
        $salida .= gridView($resultado);
    } else {
        $salida = "Error insertando el registro" . $conexion->error;
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1><?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Insertar</h1>

    <?= $menu ?>

    <?php
    if (!$_POST) {
        require "_form.php";
    }
    ?>

    <?= $salida ?>
</body>

</html>